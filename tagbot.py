import re

from disco.bot.plugin import Plugin
from disco.types.guild import GuildMember


class TagBotPlugin(Plugin):
    roles = set()
    tags_re = None

    def load(self, ctx):
        super(TagBotPlugin, self).load(ctx)
        self.roles = set([int(role_id) for role_id in self.config['tags'].keys()])
        tags_list_re = "|".join(re.escape(tag) for tag in self.config['tags'].values())
        self.tags_re = re.compile("^({})*(.+)$".format(tags_list_re))

    @Plugin.listen('GuildMemberUpdate')
    def on_guild_member_update(self, event):
        member = event.member
        taggable = self.roles.intersection(member.roles)
        tags = [self.config['tags'][str(tag)] for tag in taggable]
        clean_nick = self.get_clean_nick(member)
        if tags:
            nick = "{} {}".format("".join(tags), clean_nick)
        else:
            nick = clean_nick

        if nick != member.name and len(nick) <= 32:
            member.set_nickname(nick)

    def get_clean_nick(self, member: GuildMember):
        tags = self.tags_re.match(member.name)
        return tags.group(2).strip()
