"""
README
------

Please note this file is NOT the main entry point for this bot,
UNLESS you want to use Sentry as an exception logger.

If you want to run this bot, have a look to Disco library documentation:
https://b1naryth1ef.github.io/disco/bot_tutorial/first_steps.html

If you're not running Sentry, the config.json sentry_dsn should remain empty.

More informations about Sentry are available on their website:
https://sentry.io/
"""

from gevent.hub import Hub
from disco import cli
from raven import Client


IGNORE_ERROR = Hub.SYSTEM_ERROR + Hub.NOT_ERROR


def register_sentry_error_handler(sentry):
    Hub._origin_handle_error = Hub.handle_error

    def custom_handle_error(self, context, type, value, tb):
        if not issubclass(type, IGNORE_ERROR):
            sentry.captureException()
        self._origin_handle_error(context, type, value, tb)

    Hub.handle_error = custom_handle_error


if __name__ == '__main__':
    disco = cli.disco_main()
    sentry = Client(disco.client.config.sentry_dsn)
    sentry.environment = disco.client.config.sentry_environment
    register_sentry_error_handler(sentry)
    try:
        disco.run_forever()
    except:
        sentry.captureException()

